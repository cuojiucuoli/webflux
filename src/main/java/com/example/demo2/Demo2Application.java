package com.example.demo2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo2Application {

    public static void main(String[] args) {
        System.getProperties().setProperty("reactor.schedulers.defaultPoolSize","60");

        System.getProperties().setProperty("reactor.schedulers.defaultBoundedElasticSize","60");
        SpringApplication.run(Demo2Application.class, args);
    }

}
