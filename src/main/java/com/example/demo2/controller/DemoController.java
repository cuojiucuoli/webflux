package com.example.demo2.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.concurrent.atomic.AtomicInteger;

@RestController
@RequestMapping("/test")
public class DemoController {
    public volatile AtomicInteger ai = new AtomicInteger(0);

    @RequestMapping(path = "/thread", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public String getThread() throws InterruptedException {
        System.out.println("start");
        String s = getS();
        System.out.println("end");
        System.out.println(s);
        return s;
    }

    public String getS() throws InterruptedException {
        while (true) {

        }
    }

    @RequestMapping(path = "/thread1")
    public Mono getThread1() throws InterruptedException {

        System.out.println(ai.getAndIncrement());

        Mono<String> tMono = Mono.fromSupplier(() -> {
            try {
                System.out.println(ai.getAndIncrement());
                Thread.sleep(1000000);
                return "1";
            } catch (Exception e) {
                return "1";
            }
        }).publishOn(Schedulers.newParallel("yao"));
        return tMono;

    }

/*
    private Mono getA() throws InterruptedException {
        Mono.fromSupplier(() ->{
            try {
                Thread.sleep(100000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        })

        return Mono.just(1);
    }
*/

}
